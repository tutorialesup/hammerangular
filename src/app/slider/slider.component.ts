import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {
  cards = [{
    color: '#6657FF',
    name: '1'
  }, {
    color: '#4A9BFF',
    name: '2'
  }, {
    color: '#4361E8',
    name: '3'
  }, {
    color: '#7E43E8',
    name: '4'
  }, {
    color: '#BC4AFF',
    name: '5'
  }, {
    color: '#FF42B6',
    name: '6'
  }];
  spacecrr = 0;
  spacefn = '';

  constructor() { }

  ngOnInit() {
  }
  swipeleft(event) {
    console.log('<--');
    if (this.spacecrr < ((this.cards.length - 1) * 100)) {
      this.spacecrr += 100;
      this.spacefn = '-' + this.spacecrr + 'vw';
    } else {
      this.spacecrr = 0;
      this.spacefn = '0vw';
    }

  }
  swiperight(event) {
    console.log('-->');
    if (this.spacecrr > 0 && this.spacecrr <= ((this.cards.length - 1) * 100)) {
      this.spacecrr -= 100;
      this.spacefn = '-' + this.spacecrr + 'vw';
    }

  }

}

