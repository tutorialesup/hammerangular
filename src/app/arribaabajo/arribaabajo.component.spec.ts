import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArribaabajoComponent } from './arribaabajo.component';

describe('ArribaabajoComponent', () => {
  let component: ArribaabajoComponent;
  let fixture: ComponentFixture<ArribaabajoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArribaabajoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArribaabajoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
