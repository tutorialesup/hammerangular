import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SliderComponent } from './slider/slider.component';
import { ArribaabajoComponent } from './arribaabajo/arribaabajo.component';

const routes: Routes = [
  { path: '',  component: SliderComponent},
  { path: 'arriba',  component: ArribaabajoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
