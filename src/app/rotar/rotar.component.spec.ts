import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RotarComponent } from './rotar.component';

describe('RotarComponent', () => {
  let component: RotarComponent;
  let fixture: ComponentFixture<RotarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RotarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RotarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
