import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SliderComponent } from './slider/slider.component';
import { RotarComponent } from './rotar/rotar.component';
import { ArribaabajoComponent } from './arribaabajo/arribaabajo.component';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import * as Hammer from 'hammerjs';

  export class HammerConfig extends HammerGestureConfig {
    overrides = <any>{
      'swipe': { direction: Hammer.DIRECTION_ALL }
    };
  }

@NgModule({
  declarations: [
    AppComponent,
    SliderComponent,
    RotarComponent,
    ArribaabajoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: HammerConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
